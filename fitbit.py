#!/usr/bin/env python
# encoding: utf-8


'''Fitbit client.

Usage:
    fitbit.py subscribe <public_member_id> <access_token>
    fitbit.py subscriptions <access_token>
    fitbit.py unsubscribe <public_member_id> <access_token> 
    fitbit.py refresh_tokens <public_member_id>
    
Options:
    -h --help           Show this screen

'''

import json
import requests

from docopt import docopt


def subscribe(public_member_id, access_token):
    '''
    200 OK - Returned if the given user is already subscribed to the stream.
    201 Created - Returned if a new subscription was created in response to your request
    405 Method Not Allowed - Returned if a GET is attempted on an endpoint where only POST can be used.
    409 Conflict - Returned if the given user is already subscribed to this stream using a different subscription ID, OR if the given subscription ID is already used to identify a subscription to a different stream.
    '''
    headers = { 'Authorization': 'Bearer ' + access_token }
    return requests.post('https://api.fitbit.com/1/user/-/activities/apiSubscriptions/' + public_member_id + '.json', headers = headers)


def get_subscriptions(access_token):
    '''
    Get a list of a user's subscriptions for your application in the format requested. You can either fetch subscriptions for a specific 
    collection or the entire list of subscriptions for the user. For best practice, make sure that your application maintains this list
    on your side and use this endpoint only to periodically ensure data consistency.
    '''
    headers = { 'Authorization': 'Bearer ' + access_token }
    return requests.get('https://api.fitbit.com/1/user/-/activities/apiSubscriptions.json', headers = headers)


def unsubscribe(public_member_id, access_token):
    '''
    Deletes a subscription for a user. A successful request will return a 204 status code and empty response body.
    '''
    headers = { 'Authorization': 'Bearer ' + access_token }
    return requests.delete('https://api.fitbit.com/1/user/-/activities/apiSubscriptions/' + public_member_id + '.json', headers = headers)


def refresh_tokens(public_member_id):
    '''
    Make query to avro backend to get activities from fitbit - it forces tokens refresh.
    '''
    headers = { 'X-Fitbit-Signature': public_member_id, 'Content-Type': 'application/json' }
    json_payload = json.dumps([{
        "collectionType": "activities",
        "date": "2010-03-01",
        "ownerId": "manual-push",
        "ownerType": "user",
        "subscriptionId": public_member_id
    }])

    return requests.post('https://nx-elb.qantasassure.com/v1/provider/notification/fitbit',
        headers = headers,
        data = json_payload)


def display_response(response):
    print 'Response body:', response.text
    print 'Response status:', response.status_code


if __name__ == '__main__':
    args = docopt(__doc__, version='Fitbit client v.0.1')

    if args['subscribe']:
        display_response(subscribe(args['<public_member_id>'], args['<access_token>']))
    elif args['unsubscribe']:
        display_response(unsubscribe(args['<public_member_id>'], args['<access_token>']))
    elif args['subscriptions']:
        display_response(get_subscriptions(args['<access_token>']))
    elif args['refresh_tokens']:
        display_response(refresh_tokens(args['<public_member_id>']))

